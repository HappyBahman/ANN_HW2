import tensorflow as tf
import numpy as np
import math
import sklearn.datasets
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from tensorflow.contrib import learn
from mpl_toolkits.mplot3d import Axes3D


def euclidean_distance_tf(a, b):
    return tf.sqrt(tf.add(tf.add(tf.pow(tf.subtract(a[0], b[0]), 2), tf.pow(tf.subtract(a[1], b[1]), 2)),
                          tf.pow(tf.subtract(a[2], b[2]), 2)))


def euclidean_distance(a, b):
    return np.sqrt((a[0] - b[0]) ** 2 +
                   (a[1] - b[1]) ** 2 +
                   (a[2] - b[2]) ** 2)


def find_sigma(cluster_centers):
    max_dist = 0
    for i, center1 in enumerate(cluster_centers):
        for j in range(i + 1, len(cluster_centers)):
            dist = euclidean_distance(center1, cluster_centers[j])
            if dist > max_dist:
                max_dist = dist
    return max_dist/math.sqrt(2 * len(cluster_centers))


batch_size = 50
learning_rate = 0.5
epochs = 100
num_of_clusters = 300

sr = np.loadtxt('SwissRoll.txt')
labels = sr[:, 3]
dataset = sr[:, 0:3]


# shuffle data
idx = np.random.permutation(len(dataset))
dataset, labels = dataset[idx], labels[idx]
# divide data into test and train
test_set = dataset[int(len(dataset) * 70/100):]
test_labels = labels[int(len(dataset) * 70/100):]

labels = labels[:int(len(dataset) * 70/100)]
dataset = dataset[:int(len(dataset) * 70/100)]

labels = np.array(
    [labels == i for i in range(4)]
).T

test_labels = np.array(
    [test_labels == i for i in range(4)]
).T


kmeans = KMeans(n_clusters=num_of_clusters).fit(dataset)

cluster_centers = kmeans.cluster_centers_
sigma = find_sigma(cluster_centers)


def gaussian_function(input_layer):
    distances = np.array([euclidean_distance(input_layer, cluster_center) for cluster_center in cluster_centers])
    output = np.exp(np.power(distances, 2)/(-2 * sigma))
    return np.float32(output)


# input and output place holders
x = tf.placeholder(tf.float32, [None, num_of_clusters])
y = tf.placeholder(tf.int32, [None, 4])

W2 = tf.Variable(tf.random_normal([num_of_clusters, 4], stddev=0.03), dtype=tf.float32, name='W2')
b2 = tf.Variable(tf.random_normal([4]), dtype=tf.float32, name='b2')

hidden_out = x
y_ = tf.add(tf.matmul(hidden_out, W2), b2)

y_clipped = tf.clip_by_value(y_, 1e-10, 0.9999999)
cross_entropy = -tf.reduce_mean(
    tf.reduce_sum(
        tf.cast(
            tf.cast(y, tf.float32) * tf.log(y_clipped) + (1 - tf.cast(y, tf.float32)) * tf.log(1 - y_clipped), tf.float32), axis=1))
optimiser = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(cross_entropy)

init_op = tf.global_variables_initializer()

correct_prediction = tf.equal(tf.cast(tf.argmax(y, 1), tf.float32), tf.cast(tf.argmax(y_, 1), tf.float32))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


# start the session
inputs = np.array([gaussian_function(point) for point in dataset])

# start the session
with tf.Session() as sess:
   # initialise the variables
   sess.run(init_op)
   total_batch = int(len(dataset) / batch_size)
   for epoch in range(epochs):
        avg_cost = 0
        for i in range(total_batch):
            batch_x, batch_y = inputs[i * batch_size: min( (i + 1) * batch_size, len(inputs))], labels[i * batch_size: min( (i + 1) * batch_size, len(inputs))]
            _, c = sess.run([optimiser, cross_entropy],
                         feed_dict={x: batch_x, y: batch_y})
            avg_cost += c / total_batch
        print("Epoch:", (epoch + 1), "cost =", "{:.3f}".format(avg_cost))
   print(sess.run(accuracy, feed_dict={x: [gaussian_function(point) for point in test_set], y: test_labels}))


# acc = np.sum(np.array([np.argmax(outputs[i]) for i in range(len(outputs))]) == test_labels)/test_labels.shape[0]
# print(acc)
