import tensorflow as tf
import numpy as np
import math
import sklearn.datasets
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from tensorflow.contrib import learn
from mpl_toolkits.mplot3d import Axes3D


def euclidean_distance_tf(a, b):
    return tf.sqrt(tf.add(tf.add(tf.pow(tf.subtract(a[0], b[0]), 2), tf.pow(tf.subtract(a[1], b[1]), 2)),
                          tf.pow(tf.subtract(a[2], b[2]), 2)))


def euclidean_distance(a, b):
    return np.sqrt((a[0] - b[0]) ** 2 +
                   (a[1] - b[1]) ** 2 +
                   (a[2] - b[2]) ** 2)


def find_sigma(cluster_centers):
    max_dist = 0
    for i, center1 in enumerate(cluster_centers):
        for j in range(i + 1, len(cluster_centers)):
            dist = euclidean_distance(center1, cluster_centers[j])
            if dist > max_dist:
                max_dist = dist
    return max_dist/math.sqrt(2 * len(cluster_centers))


# batch_size = 50
# learning_rate = 0.5
epochs = 1
num_of_clusters = 500
# num_of_points = 200
landa = 0.000001
cal_w_mode = 'online'

# dataset1 = sklearn.datasets.make_swiss_roll(n_samples=num_of_points, noise=0.0, random_state=None)
# dataset1 = dataset1[0]
# dataset2 = sklearn.datasets.make_swiss_roll(n_samples=num_of_points, noise=0.0, random_state=None)
# dataset2 = dataset2[0]
# dataset2[:, 2] = -1 * dataset2[:, 2]
# dataset2[:, 0] = -1 * dataset2[:, 0]

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(dataset1[:, 0], dataset1[:, 1], dataset1[:, 2], c='r')
# ax.scatter(dataset2[:, 0], dataset2[:, 1], dataset2[:, 2], c='b')
# plt.show()

sr = np.loadtxt('SwissRoll.txt')
labels = sr[:, 3]
dataset = sr[:, 0:3]



# dataset = np.concatenate((dataset1, dataset2), 0)
# labels = np.array([0 for i in range(num_of_points)] + [1 for i in range(num_of_points)])
# shuffle data
idx = np.random.permutation(len(dataset))
dataset, labels = dataset[idx], labels[idx]
# divide data into test and train
test_set = dataset[int(len(dataset) * 70/100):]
test_labels = labels[int(len(dataset) * 70/100):]

labels = labels[:int(len(dataset) * 70/100)]
dataset = dataset[:int(len(dataset) * 70/100)]

kmeans = KMeans(n_clusters=num_of_clusters).fit(dataset)

cluster_centers = kmeans.cluster_centers_
sigma = find_sigma(cluster_centers)


def gaussian_function(input_layer):
    distances = np.array([euclidean_distance(input_layer, cluster_center) for cluster_center in cluster_centers])
    output = np.exp(np.power(distances, 2)/(-2 * sigma))
    return np.float32(output)


def vector_gaussian_function(input_layer):
    output = []
    for point in input_layer:
        output.append(tf.convert_to_tensor(np.array(gaussian_function(point))))
    return output


def gaussian_function_tf_vectorized(input_layer, name=None):
    with tf.name_scope(name, "d_gaussian_function", [input_layer]) as name:
        y = tf.py_func(vector_gaussian_function, [input_layer], [tf.float32], name=name)
    return y


def gaussian_function_tf(input_layer, name=None):
    with tf.name_scope(name, "d_gaussian_function", [input_layer]) as name:
        y = tf.py_func(gaussian_function, [input_layer], [tf.float32], name=name)
    return y


# calculate weights for one of the output neurons
def cal_w_offline(data, labels):
    O = []
    for i in range(data.shape[0]):
        O.append(np.concatenate((gaussian_function(data[i]), [1])))
    O = np.array(O).T
    D = labels
    OOti = np.linalg.inv(np.matmul(O, O.T))
    W = np.matmul(np.matmul(D,O.T), OOti)
    return W


def cal_w_online(data, labels, epochs):
    p = np.zeros((num_of_clusters + 1, num_of_clusters + 1))
    for i in range(num_of_clusters + 1):
        p[i, i] = 1
    p = landa * p
    w = np.zeros(num_of_clusters + 1).T
    for epoch in range(epochs):
        for i in range(len(data)):
            phi = np.concatenate((gaussian_function(data[i]), [1]))
            phi = phi.T
            phi_X_p = np.matmul(phi.T, p)
            p_X_phi = np.matmul(p, phi)
            p = p - np.matmul( p_X_phi , phi_X_p) / (1 + np.matmul(phi_X_p, phi))
            g = np.matmul(p, phi)
            alpha = int(labels[i]) - np.matmul(w.T, phi)
            w = w + alpha * g
    return w


def cal_w(data, labels, epochs, mode):
    if mode == 'online':
        return cal_w_online(data, labels, epochs)
    else:
        return cal_w_offline(data, labels)


# input and output place holders
x = tf.placeholder(tf.float32, [3])
y = tf.placeholder(tf.int32, [None, 4])
# W1 and b1 have no effect whatsoever, I just wrote them to have done things by the book
W1 = tf.Variable(np.ones([3, num_of_clusters]), dtype=tf.float32, name='W1')
b1 = tf.Variable(np.zeros(20), dtype=tf.float32, name='b1')
# for the third layer:
weights2 = []
for i in range(4):
    weights2.append(cal_w(dataset, (labels == i).astype(int), epochs, cal_w_mode))

weight2 = np.array(weights2).T
W2 = tf.Variable(weight2[:weight2.shape[0] - 1], dtype=tf.float32, name='W2')
b2 = tf.Variable(weight2[weight2.shape[0] - 1], dtype=tf.float32, name='b2')

# hidden_out = tf.add(tf.matmul(x, W1), b1)
hidden_out = gaussian_function_tf(x)
# y_ = tf.nn.softmax(tf.add(tf.matmul(hidden_out, W2), b2))
y_ = tf.add(tf.matmul(hidden_out, W2), b2)

init_op = tf.global_variables_initializer()

# start the session
outputs = []
with tf.Session() as sess:
    # initialise the variables
    sess.run(init_op)
    # outputs = sess.run(y_, feed_dict={x:test_set})
    for point in test_set:
        outputs.append(sess.run(y_, feed_dict={x: point}))
        # print(sess.run(accuracy, feed_dict={x: mnist.test.images, y: mnist.test.labels}))
    #     print("Epoch:", (epoch + 1), "cost =", "{:.3f}".format(avg_cost))



# import matplotlib.cm as cm
# xx = np.arange(20)
# ys = [i+xx+(i*xx)**2 for i in range(20)]
# colors = cm.rainbow(np.linspace(0, 1, len(ys)))
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# for i in range(num_of_clusters):
#     datas = dataset[kmeans.labels_ == i]
#     ax.scatter(datas[:, 0], datas[:, 1], datas[:, 2], c=colors[i])
#     plt.show()
acc = np.sum(np.array([np.argmax(outputs[i]) for i in range(len(outputs))]) == test_labels)/test_labels.shape[0]
print(acc)
