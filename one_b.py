import tensorflow as tf
import numpy as np
import math
import sklearn.datasets
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from tensorflow.contrib import learn
from mpl_toolkits.mplot3d import Axes3D
from tensorflow.contrib.estimator.python.estimator import early_stopping
import os
from sklearn.decomposition import PCA

learning_rate = 0.01
batch_size = 50
num_of_clusters = 15
buffer_size = 1000
steps = 5000
model_dir_str = 'mod_ell' + str(num_of_clusters)
sigma_constant = 1


def euclidean_distance(a, b):
    return np.sqrt((a[0] - b[0]) ** 2 +
                   (a[1] - b[1]) ** 2 +
                   (a[2] - b[2]) ** 2)


def elliptic_euclidean_power_2(a, b, pca):
    return ((a[0] - b[0]) ** 2)/pca[0] + \
           ((a[1] - b[1]) ** 2)/pca[1] +\
           ((a[2] - b[2]) ** 2)/pca[2]


def find_sigma(cluster_centers):
    max_dist = 0
    for i, center1 in enumerate(cluster_centers):
        for j in range(i + 1, len(cluster_centers)):
            dist = euclidean_distance(center1, cluster_centers[j])
            if dist > max_dist:
                max_dist = dist
    return (max_dist/math.sqrt(2 * len(cluster_centers)) * sigma_constant) ** 2


def elliptic_gaussian_function(input_layer, sigma, cluster_centers, pca):
    distances = np.array([elliptic_euclidean_power_2(input_layer, cluster_center, component)
                          for cluster_center, component in zip(cluster_centers, pca)])
    output = np.exp(distances/(-2 * sigma))
    return np.float32(output)


def get_dataset(data_size, num_of_classes):
    # load the data:
    my_dataset = [np.load('sr_ds.npy'), np.load('sr_lb.npy')]
    labels = np.zeros(data_size)
    for i in range(num_of_classes):
        j = num_of_classes - i - 1
        idx = np.argpartition(my_dataset[1], int(j * (data_size/num_of_classes)))
        labels[idx[0: int(j * (data_size/num_of_classes))]] = i + 1
    dataset = my_dataset[0]
    labels = labels.astype(np.int32)
    kmeans = KMeans(n_clusters=num_of_clusters).fit(dataset)
    # import matplotlib.cm as cm
    # xx = np.arange(20)
    # ys = [i+xx+(i*xx)**2 for i in range(5)]
    # colors = cm.rainbow(np.linspace(0, 1, len(ys)))
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # for i in range(4):
    #     datas = dataset[labels == i]
    #     ax.scatter(datas[:, 0], datas[:, 1], datas[:, 2], c=colors[i])
    # plt.show()
    pca = PCA(n_components=1)
    components = []
    for i in range(num_of_clusters):
        pca.fit(dataset[kmeans.labels_ == i])
        components.append(pca.components_[0])
    components = np.array(components)
    cluster_centers = kmeans.cluster_centers_
    sigma = find_sigma(cluster_centers)
    dataset = np.array([elliptic_gaussian_function(point, sigma, cluster_centers, components) for point in dataset])

    # # shuffle data
    idx = np.random.permutation(len(dataset))
    dataset, labels = dataset[idx], labels[idx]
    # test data:
    test_x = dataset[int(len(dataset) * 70 / 100):]
    test_y = labels[int(len(dataset) * 70 / 100):]
    # train data:
    train_y = labels[:int(len(dataset) * 70 / 100)]
    train_x = dataset[:int(len(dataset) * 70 / 100)]

    return (train_x, train_y), (test_x, test_y)


my_dataset = get_dataset(2000, 4)


def input_train_fn():
    """An input function for training"""
    dataset, _ = np.copy(my_dataset)
    features = {'signal': dataset[0]}
    labels = dataset[1]
    # features = {}
    # for i in range(num_of_clusters):
    #     features[str(i)] = dataset[0][0:100, i]
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices((features, labels))
    # Shuffle, repeat, and batch the examples.
    dataset = dataset.shuffle(buffer_size).repeat().batch(batch_size)
    # Return the dataset.
    return dataset


def input_eval_fn():
    """An input function for training"""
    _, dataset = np.copy(my_dataset)
    features = {'signal': dataset[0]}
    # features = {}
    # for i in range(num_of_clusters):
    #     features[str(i)] = dataset[0][:, i]
    labels = dataset[1]
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices((features, labels))
    # Shuffle, repeat, and batch the examples.
    dataset = dataset.shuffle(buffer_size).repeat().batch(batch_size)
    # Return the dataset.
    return dataset


def my_model(features, labels, mode, params):
    writer = tf.summary.FileWriter('./weights')
    """DNN with three hidden layers and learning_rate=0.1."""
    # Create three fully connected layers.
    net = tf.cast(features['signal'], tf.float32)
    # net = tf.feature_column.input_layer(features, params['feature_columns'])
    # Compute logits (1 per class).
    logits = tf.layers.dense(net, params['n_classes'], activation=None)
    weights = tf.get_default_graph().get_tensor_by_name(os.path.split(logits.name)[0] + '/kernel:0')
    tf.summary.histogram('weight', weights)
    # Compute predictions.
    predicted_classes = tf.argmax(logits, 1)
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'class_ids': predicted_classes[:, tf.newaxis],
            'probabilities': tf.nn.softmax(logits),
            'logits': logits,
        }
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)

    # Compute loss.
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
    # Compute evaluation metrics.
    accuracy = tf.metrics.accuracy(labels=labels,
                                   predictions=predicted_classes,
                                   name='acc_op')
    metrics = {'accuracy': accuracy}
    tf.summary.scalar('accuracy', accuracy[1])
    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode, loss=loss, eval_metric_ops=metrics)

    # Create training op.
    assert mode == tf.estimator.ModeKeys.TRAIN

    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)


# Feature columns describe how to use the input.
# my_feature_columns = [tf.feature_column.numeric_column(key='signal')]
# my_feature_columns = []
# for i in range(num_of_clusters):
#     my_feature_columns.append(tf.feature_column.numeric_column(key=str(i)))

classifier = tf.estimator.Estimator(
    model_fn=my_model,
    params={
        # 'feature_columns': my_feature_columns,
        # Two hidden layers of 10 nodes each.
        'n_classes': 4,
    },
    model_dir=model_dir_str)

loss_hook = early_stopping.stop_if_lower_hook(classifier, "loss", 0.2, model_dir_str + 'loss_eval')
acc_hook = early_stopping.stop_if_no_increase_hook(classifier, "accuracy", 100, model_dir_str + 'acc_eval')
train_spec = tf.estimator.TrainSpec(input_fn=input_train_fn, max_steps=steps, hooks=[loss_hook, acc_hook])
eval_spec = tf.estimator.EvalSpec(input_fn=input_eval_fn, steps=1000)
results = tf.estimator.train_and_evaluate(classifier, train_spec, eval_spec)

# tf.logging.set_verbosity(False)
# for i in range(steps):
#     print(i)
#     classifier.train(
#         input_fn=input_train_fn,
#         steps=1)
#     # Evaluate the model.
#     eval_result = classifier.evaluate(input_fn=input_eval_fn, steps=1)
# print(eval_result)


predictions = classifier.predict(tf.estimator.inputs.numpy_input_fn(
        x={'signal': my_dataset[1][0]},
        num_epochs=1,
        shuffle=False
    ))
# # #
list_of_preds = list(predictions)
labels = np.array([l['class_ids'] for l in list_of_preds])
true_labels = np.array(my_dataset[1][1])


conf_matrx = tf.confusion_matrix(tf.convert_to_tensor(true_labels), tf.convert_to_tensor(labels))
sess = tf.Session()
with sess.as_default():
    conf_matrx_arr = conf_matrx.eval()
conf_matrx_arr = conf_matrx_arr/(true_labels.shape[0])
np.save('conf_matrx_arr', conf_matrx_arr)

cm = conf_matrx_arr
plt.imshow(cm, cmap=plt.cm.Blues)
plt.xlabel("Predicted labels")
plt.ylabel("True labels")
plt.xticks([], [])
plt.yticks([], [])
plt.title('Confusion matrix ')
plt.colorbar()
plt.savefig('./conf_mtrx')
plt.show()

# # Train the Model.
# classifier.train(input_fn=input_train_fn, steps=steps)
# # Evaluate the model.
# eval_result = classifier.evaluate(input_fn=input_eval_fn)
# print('\nTest set accuracy: {accuracy:0.3f}\n'.format(**eval_result))
#
# # Generate predictions from the model
# expected = [str(i) for i in range(4)]
#
# predictions = classifier.predict(input_fn=input_eval_fn)
#
# for pred_dict, expec in zip(predictions, expected):
#     template = ('\nPrediction is "{}" ({:.1f}%), expected "{}"')
#
#     class_id = pred_dict['class_ids'][0]
#     probability = pred_dict['probabilities'][class_id]
#
#     print(template.format(class_id, 100 * probability, expec))
